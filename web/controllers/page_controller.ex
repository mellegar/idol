defmodule Idol.PageController do
  use Idol.Web, :controller

  def index(conn, _params) do
    data_module = get_data_module()
    info = data_module.fetch_program_info()
    description = info["description"]
    image_url = info["program_image"]
    render conn, "index.html", description: description, image_url: image_url
  end

  def get_data_module() do
    Application.get_env(:idol, :data_module)
  end
end
