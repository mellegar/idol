defmodule Idol.ParticipantsController do
  use Idol.Web, :controller

  def index(conn, _params) do
    data_module = get_data_module()
    program_info = data_module.fetch_program_info()
    participants = extract_participants(program_info)
    render conn, "index.html", participants: participants
  end

  def show(conn, %{"name" => name}) do
    data_module = get_data_module()
    program_info = data_module.fetch_program_info()
    participants = extract_participants(program_info)
    case Enum.find(participants, fn(%{"name" => pname}) -> pname == name end) do
      nil ->
        send_resp(conn, 404, "No participant found by that name")
      participant ->
        render conn, "show.html", participant: participant
    end
  end

  def videos(conn, %{"person_tag" => person_tag} = params) do
    data_module = get_data_module()
    start = Map.get(params, "start", "0")
    |> Integer.parse()
    |> elem(0)
    body = data_module.fetch_videos(person_tag, start)
    videos = Enum.take(body["results"], 5)
    video_count = body["total_hits"]
    render conn, "videos.json", videos: videos, video_count: video_count
  end

  defp extract_participants(program_info) do
    program_info
    |> Map.get("participant_groups")
    |> Enum.find(fn(%{"name" => name}) -> name == "Idoldeltagare 2016" end)
    |> Map.get("participants")
  end

  defp get_data_module() do
    Application.get_env(:idol, :data_module)
  end
end
