defmodule Idol.ParticipantsView do
  use Idol.Web, :view

  def render("videos.json", %{videos: videos, video_count: video_count}) do
    %{
      videos: Enum.map(videos, &render_video/1), video_count: video_count
    }
  end

  def parse_description(participant) do
    Map.get(participant, "description")
    |> String.split("\r\n")
    |> Enum.filter(&(&1 != ""))
    |> Enum.map(&String.split(&1, ":", parts: 2))
  end

  def image_url(participant) do
    participant
    |> Map.get("image")
    |> Map.get("url")
  end

  defp render_video(video) do
    Map.take(video, ~w(id title description image))
  end
end
