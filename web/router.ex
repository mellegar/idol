defmodule Idol.Router do
  use Idol.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Idol do
    pipe_through :browser

    get "/", PageController, :index
    get "/participants", ParticipantsController, :index
    get "/participants/:name", ParticipantsController, :show

    get "/participants/:person_tag/videos", ParticipantsController, :videos
  end

  # Other scopes may use custom stacks.
  # scope "/api", Idol do
  #   pipe_through :api
  # end
end
