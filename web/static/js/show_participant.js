const baseVideoUrl = "http://www.tv4play.se/program/idol?video_id="

export var ShowParticipant = {
  init: function(personTag) {
    this.totalVideoCount = undefined;
    this.videosFetched = 0;
    this.personTag = personTag;
    this.loadVideos();
    this.fetchInProgress = false;
  },

  loadVideos: function() {
    if(this.fetchInProgress) {
      return;
    }
    this.startFetching();
    var that = this;
    const url = "/participants/" + this.personTag + "/videos?start=" + this.videosFetched;
    var headers = new Headers({'Content-Type': 'application/json'});
    var options = {method: 'get', headers: headers};
    fetch(url, options)
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        that.totalVideoCount = json.video_count;
        json.videos.map(video => that.appendVideo(video));
        that.endFetching();
      })
    .catch(function(e) {
      console.log("error fetching videos:", e);
    });
  },

  startFetching: function() {
    var button = document.getElementById('loadVideosButton');
    button.disabled = true;
    this.fetchInProgress = true;
  },

  endFetching: function() {
    this.fetchInProgress = false;
    var button = document.getElementById('loadVideosButton');
    if(this.videosFetched < this.totalVideoCount) {
      button.disabled = false;
    } else {
      button.innerHTML = "Inga fler videos";
    }
  },

  appendVideo: function(video) {
    var videoUrl = baseVideoUrl + video.id;
    var container = document.getElementsByClassName('participant-videos')[0];
    var videoContainer = document.createElement('div');
    var titleLink = document.createElement('a');
    var title = document.createElement('p');
    var description = document.createElement('p');
    var imageLink = document.createElement('a');
    var image = document.createElement('img');
    videoContainer.className = "video-container"
    titleLink.setAttribute('href', videoUrl);
    title.className = "video-title";
    description.className = "video-description";
    image.className = "video-image";
    title.innerHTML = video.title;
    description.innerHTML = video.description;
    imageLink.setAttribute('href', videoUrl);
    image.setAttribute('src', video.image);
    image.setAttribute('onerror', "this.style.display='none'");
    titleLink.appendChild(title);
    videoContainer.appendChild(titleLink);
    videoContainer.appendChild(description);
    imageLink.appendChild(image);
    videoContainer.appendChild(imageLink);
    container.appendChild(videoContainer);
    this.videosFetched += 1;
  }
}
