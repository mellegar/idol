defmodule Idol.Data do
  @participants_url "http://api.tv4play.se/site/programs/idol"

  def fetch_program_info() do
    HTTPoison.get!(@participants_url)
    |> Map.get(:body)
    |> Poison.decode!()
  end

  def fetch_videos(person_tag, start) do
    "http://api.tv4play.se/play/video_assets.json?tags=#{person_tag}&start=#{start}"
    |> URI.encode()
    |> HTTPoison.get!()
    |> Map.get(:body)
    |> Poison.decode!()
  end
end
