defmodule Idol.MockData do
  def fetch_program_info() do
    %{
      "program_image" => "program_image_url",
      "description" => "idol program description",
      "participant_groups" => [
        %{
          "name" => "Idoldeltagare 2016",
          "participants" => [
            %{
              "name" => "participant one",
              "person_tag" => "participant-one",
              "description" => "something: 3\r\nsomething else: 4",
              "image" => %{"url" => "url_participant1"}
             }
           ]
         }
       ]
     }
  end

  def fetch_videos("participant-one", start) do
    filler = %{"description" => "a", "id" => 5, "image" => "b", "title" => "c"}
    videos = [
      %{
        "description" => "video one participant one",
        "id" => 3,
        "image" => "url_to_image_for_video_one",
        "title" => "title video one"
      },
      %{
        "description" => "video two participant one",
        "id" => 4,
        "image" => "url_to_image_for_video_two",
        "title" => "title video two"
      },
      filler,
      filler,
      filler
    ]
    %{
      "total_hits" => 5,
      "results" => Enum.drop(videos, start)
    }
  end

  def fetch_videos("participant-one", 5) do
    %{
      "total_hits" => 5,
      "results" => []
    }
  end
end
