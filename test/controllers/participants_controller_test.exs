defmodule Idol.ParticipantsControllerTest do
  use Idol.ConnCase

  test "Render participants names", %{conn: conn} do
    conn = get conn, "/participants"
    assert html_response(conn, 200) =~ "participant one"
  end

  test "Render participants images", %{conn: conn} do
    conn = get conn, "/participants"
    assert html_response(conn, 200) =~ "url_participant1"
  end

  test "Render participant name", %{conn: conn} do
    conn = get conn, "/participants/participant one"
    assert html_response(conn, 200) =~ "participant one"
  end

  test "Render participant description", %{conn: conn} do
    conn = get conn, "/participants/participant one"
    assert html_response(conn, 200) =~ "something else"
  end

  test "Fetch images", %{conn: conn} do
    conn = get conn, "/participants/participant-one/videos"
    response = json_response(conn, 200)
    assert Map.get(response, "video_count") == 5
    assert length(Map.get(response, "videos")) == 5
  end

  test "Fetch some images", %{conn: conn} do
    conn = get conn, "/participants/participant-one/videos?start=3"
    response = json_response(conn, 200)
    assert Map.get(response, "video_count") == 5
    assert length(Map.get(response, "videos")) == 2
  end

  test "Fetch no images", %{conn: conn} do
    conn = get conn, "/participants/participant-one/videos?start=5"
    response = json_response(conn, 200)
    assert Map.get(response, "video_count") == 5
    assert length(Map.get(response, "videos")) == 0
  end
end
