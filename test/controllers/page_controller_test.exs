defmodule Idol.PageControllerTest do
  use Idol.ConnCase

  test "Render program description", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "idol program description"
  end

  test "Render program image", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "program_image_url"
  end
end
