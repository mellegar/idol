defmodule Idol.IntegrationTest do
  use Idol.ConnCase

  @moduletag :integration

  setup_all do
    original_data_module = Application.get_env(:idol, :data_module)
    Application.put_env(:idol, :data_module, Idol.Data)
    on_exit fn ->
      Application.put_env(:idol, :data_module, original_data_module)
    end
    :ok
  end

  test "Render program description", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Succéprogrammet Idol, med juryn i spetsen, ger sig ut på jakt efter Sveriges nästa popstjärna."
  end

  test "Render program image", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "https://d1fc69ngj1uuz1.cloudfront.net/4fc630da04bf725f69000053/tv4se-play.png?2"
  end

  test "Render participants names", %{conn: conn} do
    conn = get conn, "/participants"
    assert html_response(conn, 200) =~ "Adrian Jonasson"
  end

  test "Render participants images", %{conn: conn} do
    conn = get conn, "/participants"
    assert html_response(conn, 200) =~ "https://d1fc69ngj1uuz1.cloudfront.net/57e59656b9a9f6871300010a/14798753_10154016519946814_1616691675_n.jpg?4"
  end

  test "Render participant name", %{conn: conn} do
    conn = get conn, "/participants/Adrian Jonasson"
    assert html_response(conn, 200) =~ "Adrian Jonasson"
  end

  test "Render participant description", %{conn: conn} do
    conn = get conn, "/participants/Adrian Jonasson"
    assert html_response(conn, 200) =~ "Är uppvuxen i Brunnsbo, Göteborg."
  end

  test "Fetch images", %{conn: conn} do
    conn = get conn, "/participants/adrian-jonasson/videos"
    response = json_response(conn, 200)
    assert Map.get(response, "video_count") > 10
    assert length(Map.get(response, "videos")) == 5
  end

  test "Fetch no images", %{conn: conn} do
    conn = get conn, "/participants/adrian-jonasson/videos?start=500"
    response = json_response(conn, 200)
    assert Map.get(response, "video_count") > 10
    assert length(Map.get(response, "videos")) == 0
  end
end
