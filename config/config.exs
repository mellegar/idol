# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :idol,
  ecto_repos: [Idol.Repo]

# Configures the endpoint
config :idol, Idol.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "dn+rQ0V06eNkLF4yALBnXzG4t4v2OkndJsfJg1fyAygWPNQiO9F1nHcES4zoGnmZ",
  render_errors: [view: Idol.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Idol.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :idol,
  data_module: Idol.Data

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
