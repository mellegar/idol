# Idol

Requirements:

  * Erlang 19.0
  * Elixir 1.4.2 (probably any 1.4 version, but can't guarantee anything)

To start the idol app:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`
  * Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

To run the tests:

  * mix test

By default the integration tests are excluded. To run the integration tests:

  * mix test --only integration

To run all tests:

  * mix test --include integration
